from click.testing import CliRunner

from pfasdr.cli.main import main


def test_main():
    # Mock
    runner = CliRunner()

    # Test
    result = runner.invoke(main)

    # Assert
    assert result.exit_code == 0
    assert result.output.startswith('Usage: ')
