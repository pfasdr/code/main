import os

from datetime import datetime

from pathlib import Path

ROOT_PATH = Path(__file__).parent.parent.parent

# Configuration of the run under investigation
_GRID_NAME = '15-node-dynsim-benchmark'
_LAST_SAMPLING_DATE = '2018-03-01'
#_LAST_TRAINING_DATE_TIME = '2018-04-21.00_33_34'
# _LAST_TRAINING_DATE_TIME = '2018-04-19.19_32_30'
# _LAST_TRAINING_DATE_TIME = '2018-04-21.20_30_39'
# _LAST_TRAINING_DATE_TIME = '2018-04-25.00_28_25/'
# _LAST_TRAINING_DATE_TIME = '2018-04-25.21_33_15/'
_LAST_TRAINING_DATE_TIME = '2018-04-25.23_34_25/'

# Auto-Configuration of current run
_CURRENT_DATE_TIME = datetime.utcnow().strftime("%Y-%m-%d.%H_%M_%S")
_GRID_PATH = os.path.join(ROOT_PATH, '..', 'PFASDR.Data.15-node-dynsim-benchmark')
_LAST_SAMPLING_PATH = os.path.join(_GRID_PATH, 'by sampling', _LAST_SAMPLING_DATE)

# Paths for outside usage
GRID_FILE_PATH = os.path.join(_GRID_PATH, 'model', 'grid.dgs')

LAST_SITUATIONS_PATH = os.path.join(_LAST_SAMPLING_PATH, 'situations')
#ensure_path(LAST_SITUATIONS_PATH)

LAST_TFRECORD_FILE_PATHS = os.path.join(_LAST_SAMPLING_PATH, 'situations')

LAST_CHECKPOINT_PATH = os.path.join(_LAST_SAMPLING_PATH, 'by training', _LAST_TRAINING_DATE_TIME, 'checkpoints')
#ensure_path(LAST_CHECKPOINT_PATH)

#LAST_GENERATOR_MODEL_CHECKPOINT_FILE_PATH = os.path.join(LAST_CHECKPOINT_PATH, 'generator_step_399.h5')
LAST_GENERATOR_MODEL_CHECKPOINT_FILE_PATH = os.path.join(LAST_CHECKPOINT_PATH, 'generator_step_100.h5')

CURRENT_CHECKPOINT_PATH = os.path.join(_LAST_SAMPLING_PATH, 'by training', _CURRENT_DATE_TIME, 'checkpoints')
#ensure_path(CURRENT_CHECKPOINT_PATH)

LAST_TRAINING_GRAPHS_PATH = os.path.join(_LAST_SAMPLING_PATH, 'by training', _LAST_TRAINING_DATE_TIME, 'graphs')
#ensure_path(LAST_TRAINING_GRAPHS_PATH)

# binary classifier
TRAINING_DATA_PROGRESS_FILE_PATH = os.path.join(CURRENT_CHECKPOINT_PATH, 'progress.csv')

# SeqGAN
EXPERIMENT_LOG_FILE_PATH = os.path.join(CURRENT_CHECKPOINT_PATH, 'experiment-log.txt')
DATA_EVAL_FILE_PATH = os.path.join(CURRENT_CHECKPOINT_PATH, 'data_eval.txt')
DATA_REAL_FILE_PATH = os.path.join(CURRENT_CHECKPOINT_PATH, 'data_real.txt')
DATA_FAKE_FILE_PATH = os.path.join(CURRENT_CHECKPOINT_PATH, 'data_fake.txt')

SEQGAN_TARGET_PARAMS_FILE_PATH = os.path.join(ROOT_PATH,
                                              'pfasdr', 'experiments', 'DynSim',
                                              'SeqGAN', 'save',
                                              'target_params_py3.pkl')
ROOT_DATA_PANDAPOWER_LINEAR_PATH = \
    ROOT_PATH / 'data' / 'pandapower' / 'linear_grid'
