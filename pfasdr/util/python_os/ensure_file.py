from os import utime


def ensure_file(file_path: str) -> None:
    """
    Make sure there is a file at the given file path and update its update time. Equivalent to the GNU tool touch.

    :param file_path:
    :return:
    """
    with open(file_path, 'a'):
        utime(path=file_path, times=None)
