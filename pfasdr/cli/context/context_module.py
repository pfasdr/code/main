class Context(object):
    def __init__(
        self,
        log_level: int,
    ):
        self.debug = log_level
