import click

from pfasdr.cli.arguments.determine_grids import determine_grids
from pfasdr.cli.commands.learn import learn_constraints
from pfasdr.cli.commands.monitor import monitor_training
from pfasdr.cli.commands.sample import sample_grid
from pfasdr.cli.commands.test import run_test_suite
from pfasdr.cli.context.context_module import Context


@click.group()
@click.option(
    '--log_level',
    default=0,
    type=int,
    envvar='PFASDR_LOG_LEVEL',
)
@click.pass_context
def main(
    ctx,
    log_level,
):
    ctx.obj = Context(
        log_level=log_level
    )


@main.command()
@click.argument(
    'skip',
    required=False,
    default=False,
    type=bool,
)
def monitor(
    *,
    skip: bool,
):
    monitor_training(
        skip=skip,
    )


@main.command()
@click.argument(
    'skip',
    required=False,
    default=False,
    type=bool,
)
def test(
    *,
    skip,
):
    run_test_suite(skip=skip)


@main.command()
@click.argument(
    'grid',
    required=True,
    type=click.Choice(determine_grids()),
)
@click.argument(
    'skip',
    required=False,
    default=False,
    type=bool,
)
def learn(
    *,
    grid: str,
    skip: bool,
):
    learn_constraints(
        grid=grid,
        skip=skip,
    )


@main.command()
@click.argument(
    'grid',
    required=True,
    type=click.Choice(
        determine_grids()
    ),
)
@click.argument(
    'skip',
    required=False,
    default=False,
    type=bool,
)
def sample(
    *,
    grid: str,
    skip: bool,
):
    sample_grid(
        grid=grid,
        skip=skip,
    )


if __name__ == '__main__':
    main()
